﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDAS.Models.Common;
using BDAS.Repository;

namespace BDAS.Controllers
{
    [ApiController]
    public abstract class BaseController<T> : ControllerBase where T : Model
    {
        public IRepository<T> Repository { get; set; }

        protected BaseController(IRepository<T> repository) => Repository = repository;

        [HttpPost("")]
        public async Task<IActionResult> InsertMany([FromBody] List<T> items)
        {
            var results = await Repository.InsertManyAsync(items);
            results.Result = results.Result.Take(10);
            return Ok(results);
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] T item)
        {
            await Repository.UpdateAsync(item);
            return Ok(item);
        }

        [HttpDelete("")]
        public async Task<IActionResult> DeleteMany()
        {
            var ids = new List<string>();
            var results = await Repository.DeleteManyAsync(ids);
            return Ok(results);
        }

        [HttpGet("")]
        public async Task<IActionResult> GetMany()
        {
            var results = await Repository.GetManyAsync();
            results.Result = results.Result.Take(10);
            return Ok(results);
        }
    }
}
