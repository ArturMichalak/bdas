﻿using BDAS.Models.Documents;
using BDAS.Repository;

namespace BDAS.Controllers.DocControllers
{
    public class ClientController : DocController<ClientDocument>
    {
        public ClientController(IRepository<ClientDocument> repository) : base(repository) { }
    }
}
