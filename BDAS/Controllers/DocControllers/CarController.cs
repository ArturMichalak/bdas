﻿using System.Diagnostics;
using System.Threading.Tasks;
using BDAS.Models.Common;
using BDAS.Models.Documents;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace BDAS.Controllers.DocControllers
{
    public class CarController : DocController<CarDocument>
    {
        public CarController(IRepository<CarDocument> repository) : base(repository) {}

        [HttpGet("model/{model}")]
        public async Task<TimerModel<CarDocument>> FindByModel(string model)
        {
            var timer = new Stopwatch();
            timer.Start();
            var result = await Repository.GetCollection().Find(it => it.Model == model).FirstOrDefaultAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<CarDocument>(result, timer.Elapsed.TotalMilliseconds);
        }
    }
}
