﻿using BDAS.Models.Documents;
using BDAS.Repository;

namespace BDAS.Controllers.DocControllers
{
    public class SellerController : DocController<SellerDocument>
    {
        public SellerController(IRepository<SellerDocument> repository) : base(repository) { }
    }
}
