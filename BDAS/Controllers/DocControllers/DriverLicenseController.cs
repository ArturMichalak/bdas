﻿using BDAS.Models.Documents;
using BDAS.Repository;

namespace BDAS.Controllers.DocControllers
{
    public class DriverLicenseController : DocController<DriverLicenseDocument>
    {
        public DriverLicenseController(IRepository<DriverLicenseDocument> repository) : base(repository) { }
    }
}
