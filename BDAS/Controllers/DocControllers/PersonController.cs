﻿using BDAS.Models.Documents;
using BDAS.Repository;

namespace BDAS.Controllers.DocControllers
{
    public class PersonController : DocController<PersonDocument>
    {
        public PersonController(IRepository<PersonDocument> repository) : base(repository) { }
    }
}
