﻿using BDAS.Models.Common;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;

namespace BDAS.Controllers.DocControllers
{
    [Route("doc/[controller]")]
    public abstract class DocController<T> : BaseController<T> where T : Document
    {
        protected DocController(IRepository<T> repository) : base(repository) { }
    }
}
