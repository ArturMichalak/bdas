﻿using System.Diagnostics;
using System.Threading.Tasks;
using BDAS.Models.BasicModels;
using BDAS.Models.Common;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BDAS.Controllers.ObjControllers
{
    public class CarController : ObjController<CarModel>
        {
        public CarController(IRepository<CarModel> repository) : base(repository) { }

        [HttpGet("model/{model}")]
        public async Task<TimerModel<CarModel>> FindByModel(string model)
        {
            var timer = new Stopwatch();
            timer.Start();
            var result = await Repository.GetDbSet().FirstOrDefaultAsync(x => x.Model == model);
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<CarModel>(result, timer.Elapsed.TotalMilliseconds);
        }
    }
}
