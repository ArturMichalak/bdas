﻿using System.Linq;
using System.Threading.Tasks;
using BDAS.Models.Common;
using BDAS.Models.ObjModels;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BDAS.Controllers.ObjControllers
{
    public class ClientController : ObjController<ClientModel>
    {
        public ClientController(IRepository<ClientModel> repository) : base(repository) { }

        [HttpGet("join")]
        public new async Task<IActionResult> GetMany()
        {
            return Ok(await Repository.GetDbSet().Include(x => x.Name).ToListAsync());
        }
    }
}
