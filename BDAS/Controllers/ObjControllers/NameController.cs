﻿using BDAS.Models.ObjModels;
using BDAS.Repository;

namespace BDAS.Controllers.ObjControllers
{
    public class NameController : ObjController<NameModel>
    {
        public NameController(IRepository<NameModel> repository) : base(repository) { }
    }
}
