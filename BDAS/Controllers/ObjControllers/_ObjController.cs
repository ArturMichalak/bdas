﻿using BDAS.Models.Common;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;

namespace BDAS.Controllers.ObjControllers
{
    [Route("obj/[controller]")]
    public abstract class ObjController<T> : BaseController<T> where T : IdModel
    {
        protected ObjController(IRepository<T> repository) : base(repository) {}
    }
}
