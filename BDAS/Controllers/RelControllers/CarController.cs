﻿using BDAS.Models.BasicModels;
using BDAS.Repository;

namespace BDAS.Controllers.RelControllers
{
    public class CarController : _RelController<CarModel>
    {
        public CarController(IRepository<CarModel> repository) : base(repository) { }
    }
}
