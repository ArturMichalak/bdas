﻿using BDAS.Models.Common;
using BDAS.Repository;
using Microsoft.AspNetCore.Mvc;

namespace BDAS.Controllers.RelControllers
{
    [Route("rel/[controller]")]
    public abstract class _RelController<T> : BaseController<T> where T : IdModel
    {
        protected _RelController(IRepository<T> repository) : base(repository) { }
    }
}
