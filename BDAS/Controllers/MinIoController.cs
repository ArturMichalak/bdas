﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using BDAS.Models.Input;
using Microsoft.AspNetCore.Mvc;
using Minio;
using Minio.DataModel;

namespace BDAS.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MinIoController : ControllerBase
    {
        public MinioClient MinIoClient { get; set; }
        private const string BucketName = "media";

        public MinIoController(MinioClient minIoClient)
        {
            MinIoClient = minIoClient;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetList()
        {
            var exists = await MinIoClient.BucketExistsAsync(BucketName);
            if (!exists) return NotFound();
            try
            {
                var observable = MinIoClient.ListObjectsAsync(BucketName);

                var items = new List<Item>();
                var subscription = observable.Subscribe(
                    item => items.Add(item),
                    ex => Console.WriteLine($"OnError: {ex}"));
                observable.Wait();
                subscription.Dispose();
                return Ok(items);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Upload([FromForm] MinIoUpload model)
        {
            await MinIoClient.PutObjectAsync(BucketName, model.File.FileName, model.File.OpenReadStream(), model.File.Length, model.File.ContentType);
            return Ok();
        }
    }
}
