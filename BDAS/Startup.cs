using BDAS.Models.BasicModels;
using BDAS.Models.Context;
using BDAS.Models.Documents;
using BDAS.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Minio;
using MongoDB.Driver;
using Microsoft.EntityFrameworkCore;
using ClientModel = BDAS.Models.ObjModels.ClientModel;
using NameModel = BDAS.Models.ObjModels.NameModel;

namespace BDAS
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(
                new MinioClient(
                    _configuration["MinIO:Endpoint"],
                    _configuration["MinIO:Login"],
                    _configuration["MinIO:Password"])
            );

            services.AddDbContext<ObjContext>(ops => 
                ops.UseNpgsql(_configuration.GetConnectionString("Obj")));

            services.AddDbContext<RelContext>(ops =>
               ops.UseSqlServer(_configuration.GetConnectionString("Rel")));

            services.AddSingleton(new MongoClient(_configuration.GetConnectionString("Doc")));

            // MongoContext repository
            services.AddScoped(typeof(IRepository<CarDocument>), x =>
                new DocRepository<CarDocument>(x.GetRequiredService<MongoClient>()));

            services.AddScoped(typeof(IRepository<PersonDocument>), x =>
                new DocRepository<PersonDocument>(x.GetRequiredService<MongoClient>()));

            services.AddScoped(typeof(IRepository<ClientDocument>), x =>
                new DocRepository<ClientDocument>(x.GetRequiredService<MongoClient>()));

            services.AddScoped(typeof(IRepository<SellerDocument>), x =>
                new DocRepository<SellerDocument>(x.GetRequiredService<MongoClient>()));

            services.AddScoped(typeof(IRepository<DriverLicenseDocument>), x =>
                new DocRepository<DriverLicenseDocument>(x.GetRequiredService<MongoClient>()));

            // ObjContext repository
            services.AddScoped(typeof(IRepository<CarModel>), x =>
                new ObjRepository<CarModel>(x.GetRequiredService<ObjContext>()));

            services.AddScoped(typeof(IRepository<NameModel>), x =>
                new ObjRepository<NameModel>(x.GetRequiredService<ObjContext>()));

            services.AddScoped(typeof(IRepository<ClientModel>), x =>
                new ObjRepository<ClientModel>(x.GetRequiredService<ObjContext>()));

            services.AddControllers();

            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}
