﻿CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS CAR (
	id uuid DEFAULT uuid_generate_v4 (),
	model text NOT NULL,
	first_registration date NOT NULL,
	mileage oid NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS NAME (
	id uuid DEFAULT uuid_generate_v4 (),
	name text NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS PERSON (
	surname text NOT NULL
);

CREATE TABLE IF NOT EXISTS CLIENT (
	id uuid DEFAULT uuid_generate_v4 (),
	balance decimal NOT NULL,
	favorite_brand text NOT NULL,
	PRIMARY KEY(id)
) INHERITS (PERSON);

CREATE TABLE IF NOT EXISTS SELLER (
	id uuid DEFAULT uuid_generate_v4 (),
	contact_number text NOT NULL,
	PRIMARY KEY(id)
) INHERITS (PERSON);

CREATE TABLE IF NOT EXISTS CLIENTS_NAMES (
	id uuid DEFAULT uuid_generate_v4 (),
	person_id uuid NOT NULL,
	name_id uuid NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_client FOREIGN KEY(person_id) REFERENCES client(id),
	CONSTRAINT fk_name FOREIGN KEY(name_id) REFERENCES name(id)
);

CREATE TABLE IF NOT EXISTS SELLERS_NAMES (
	id uuid DEFAULT uuid_generate_v4 (),
	person_id uuid NOT NULL,
	name_id uuid NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_seller FOREIGN KEY(person_id) REFERENCES seller(id),
	CONSTRAINT fk_name FOREIGN KEY(name_id) REFERENCES name(id)
);

CREATE TABLE IF NOT EXISTS DRIVER_LICENSE (
	id uuid DEFAULT uuid_generate_v4 (),
	entitlement_issue_date date NOT NULL,
	expiration_date date NOT NULL,
	type text NOT NULL,
	PRIMARY KEY(id)
);

# insert examples

INSERT INTO public.client(
	surname, balance, favorite_brand)
	VALUES ("Marcinkowski", 150000, "BMW");

INSERT INTO public.car(
	model, first_registration, mileage)
	VALUES ("Opel Corsa", "2004-05-22", 280450);