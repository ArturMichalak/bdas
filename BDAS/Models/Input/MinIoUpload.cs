﻿using Microsoft.AspNetCore.Http;

namespace BDAS.Models.Input
{
    public class MinIoUpload
    {
        public IFormFile File { get; set; }
    }
}
