﻿using BDAS.Models.BasicModels;
using Microsoft.EntityFrameworkCore;

namespace BDAS.Models.Context
{
    public class RelContext: DbContext
    {
        public DbSet<CarModel> Cars { get; set; }

        public RelContext(DbContextOptions<RelContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // relations here
            //
            base.OnModelCreating(modelBuilder);
        }
    }
}
