﻿using BDAS.Models.BasicModels;
using Microsoft.EntityFrameworkCore;
using BDAS.Models.ObjModels;
using NameModel = BDAS.Models.ObjModels.NameModel;

namespace BDAS.Models.Context
{
    public class ObjContext: DbContext
    {
        public DbSet<ClientModel> Clients { get; set; }
        public DbSet<NameModel> Names { get; set; }
        public DbSet<CarModel> Cars { get; set; }
        public ObjContext(DbContextOptions<ObjContext> options): base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region relations
            modelBuilder.Entity<ClientModel>()
                .HasMany(p => p.DriverLicenses)
                .WithOne(l => l.Client);

            modelBuilder.Entity<ClientModel>()
                .HasMany(p => p.Name)
                .WithMany(n => n.Client)
                .UsingEntity(j => j.ToTable("clients_names"));
            #endregion

            base.OnModelCreating(modelBuilder);
        }
    }
}
