﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BDAS.Models.ObjModels
{
    [Table("client")]
    public class ClientModel : PersonModel
    {
        [Required, Column("balance")] public decimal Balance { get; set; }
        [Required, Column("favorite_brand")] public string FavoriteBrand { get; set; }
        public ICollection<NameModel> Name { get; set; }
    }
}
