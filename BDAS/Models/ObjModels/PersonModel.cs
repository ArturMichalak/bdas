﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BDAS.Models.ObjModels
{
    [Table("person")]
    public class PersonModel : BasicModels.PersonModel
    {
        public ICollection<DriverLicenseModel> DriverLicenses { get; set; }
    }
}
