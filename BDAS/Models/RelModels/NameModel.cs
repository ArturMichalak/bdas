﻿using System.Collections.Generic;

namespace BDAS.Models.RelModels
{
    public class NameModel: BasicModels.NameModel
    {
        public ICollection<PersonModel> People { get; set; }
    }
}
