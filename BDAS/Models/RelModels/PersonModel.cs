﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BDAS.Models.Common;

namespace BDAS.Models.RelModels
{
    public class PersonModel: IdModel
    {
        [Required, MinLength(2), Column("surname")] public string Surname { get; set; }
        public ICollection<NameModel> Names { get; set; }
    }
}
