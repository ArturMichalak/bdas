﻿namespace BDAS.Models.Common
{
    public class TimerModel<T>
    {
        public TimerModel(T result, double time)
        {
            Result = result;
            Time = time;
        }

        public TimerModel(double time)
        {
            Time = time;
        }

        public T Result { get; set; }
        public double Time { get; set; }
    }
}
