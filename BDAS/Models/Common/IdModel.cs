﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BDAS.Models.Common
{
    public class IdModel : Model
    {
        [Key, Column("id"), DatabaseGenerated(DatabaseGeneratedOption.Identity)] public Guid Id { get; set; }
    }
}
