﻿using BDAS.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace BDAS.Models.Documents
{
    [BsonCollection("Client")]
    public class ClientDocument: Common.Document
    {
        [BsonElement("balance")] public decimal Balance { get; set; }
        [BsonElement("favorite_brand")] public string FavoriteBrand { get; set; }
    }
}
