﻿using System.ComponentModel.DataAnnotations;
using BDAS.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace BDAS.Models.Documents
{
    [BsonCollection("Seller")]
    public class SellerDocument : Common.Document
    {
        [Required, Phone, BsonElement("contact_number")] public string ContactNumber { get; set; }
    }
}
