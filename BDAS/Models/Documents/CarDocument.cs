﻿using System;
using System.ComponentModel.DataAnnotations;
using BDAS.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace BDAS.Models.Documents
{
    [BsonCollection("Car")]
    public class CarDocument: Common.Document
    {
        [Required, BsonElement("model")] public string Model { get; set; }
        [BsonElement("first_registration")] public DateTime FirstRegistration { get; set; } = DateTime.Today;
        [BsonElement("mileage")] public uint Mileage { get; set; }
    }
}
