﻿using System;
using BDAS.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace BDAS.Models.Documents
{
    [BsonCollection("DriverLicense")]
    public class DriverLicenseDocument: Common.Document
    {
        [BsonElement("entitlement_issue_date")] public DateTime EntitlementIssueDate { get; set; }
        [BsonElement("expiration_date")] public DateTime ExpirationDate { get; set; }
        [BsonElement("type")] public string Type { get; set; }
    }
}
