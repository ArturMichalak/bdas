﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BDAS.Attributes;
using MongoDB.Bson.Serialization.Attributes;

namespace BDAS.Models.Documents
{
    [BsonCollection("Person")]
    public class PersonDocument: Common.Document
    {
        [Required, MinLength(1), BsonElement("names")] public IList<string> Names { get; set; }
        [Required, MinLength(2), BsonElement("surname")] public string Surname { get; set; }
    }
}
