﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using BDAS.Models.Common;

namespace BDAS.Models.BasicModels
{
    [Table("driver_license")]
    public abstract class DriverLicenseModel : IdModel
    {
        public DateTime EntitlementIssueDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string Type { get; set; }
    }
}
