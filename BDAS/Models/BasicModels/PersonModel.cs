﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BDAS.Models.Common;

namespace BDAS.Models.BasicModels
{
    [Table("person")]
    public abstract class PersonModel : IdModel
    {
        [Required, MinLength(2), Column("surname")] public string Surname { get; set; }
    }
}
