﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BDAS.Models.Common;

namespace BDAS.Models.BasicModels
{
    [Table("car")]
    public class CarModel : IdModel
    {
        [Required, Column("model")] public string Model { get; set; }
        [Column("first_registration")] public DateTime FirstRegistration { get; set; } = DateTime.Today;
        [Column("mileage")] public uint Mileage { get; set; }
    }
}
