﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BDAS.Models.BasicModels
{
    [Table("seller")]
    public abstract class SellerModel : PersonModel
    {
        [Required, Phone] public string ContactNumber { get; set; }
    }
}
