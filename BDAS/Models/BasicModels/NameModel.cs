﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BDAS.Models.Common;

namespace BDAS.Models.BasicModels
{
    [Table("name")]
    public abstract class NameModel : IdModel
    {
        [MinLength(2), Column("name")] public string Name { get; set; }
    }
}
