﻿using BDAS.Models.Common;
using BDAS.Models.Context;

namespace BDAS.Repository
{
    public class RelRepository<T> : Repository<T, RelContext> where T : IdModel
    {
        public RelRepository(RelContext context) : base(context) { }

    }
}
