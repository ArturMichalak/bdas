﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDAS.Models.Common;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace BDAS.Repository
{
    public interface IRepository<T> where T : Model
    {
        Task<TimerModel<IEnumerable<T>>> GetManyAsync();
        Task<TimerModel<T>> UpdateAsync(T item);
        Task<TimerModel<IEnumerable<T>>> InsertManyAsync(IEnumerable<T> items);
        Task<TimerModel<IEnumerable<string>>> DeleteManyAsync(IEnumerable<string> ids);
        DbSet<T> GetDbSet();
        DbContext GetDbContext();
        IMongoCollection<T> GetCollection();
    }
}
