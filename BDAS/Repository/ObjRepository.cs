﻿using BDAS.Models.Common;
using BDAS.Models.Context;

namespace BDAS.Repository
{
    public class ObjRepository<T> : Repository<T, ObjContext> where T : IdModel
    {
        public ObjRepository(ObjContext context): base(context) {}
    }
}
