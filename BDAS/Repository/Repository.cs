﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BDAS.Models.BasicModels;
using BDAS.Models.Common;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace BDAS.Repository
{
    public class Repository<T, TD> : IRepository<T> where T : IdModel where TD : DbContext
    {
        private readonly TD _context;

        public Repository(TD context) => _context = context;

        public async Task<TimerModel<IEnumerable<T>>> GetManyAsync()
        {
            var timer = new Stopwatch();
            timer.Start();
            var results = await _context.Set<T>().ToListAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<T>>(results, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<T>> UpdateAsync(T item)
        {
            var timer = new Stopwatch();
            timer.Start();
            _context.Set<T>().Update(item);
            await _context.SaveChangesAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<T>(item, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<IEnumerable<T>>> InsertManyAsync(IEnumerable<T> items)
        {
            var timer = new Stopwatch();
            timer.Start();
            await _context.Set<T>().AddRangeAsync(items);
            await _context.SaveChangesAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<T>>(items, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<IEnumerable<string>>> DeleteManyAsync(IEnumerable<string> ids)
        {
            var timer = new Stopwatch();
            timer.Start();
            var items = await _context.Set<T>().Where(x => true).ToListAsync();
            _context.Set<T>().RemoveRange(items);
            await _context.SaveChangesAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<string>>(ids, timer.Elapsed.TotalMilliseconds);
        }

        public DbSet<T> GetDbSet()
        {
            return _context.Set<T>();
        }

        public DbContext GetDbContext()
        {
            return _context;
        }

        public IMongoCollection<T> GetCollection()
        {
            throw new System.NotImplementedException();
        }

        public Task<TimerModel<T>> FindBySurname(string surname)
        {
            throw new System.NotImplementedException();
        }
    }
}
