﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BDAS.Attributes;
using BDAS.Models.Common;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;

namespace BDAS.Repository
{
    public class DocRepository<T> : IRepository<T> where T : Document
    {
        public IMongoCollection<T> Collection { get; }

        public DocRepository(IMongoClient client)
        {
            var db = client.GetDatabase("MDB");
            var collectionName = GetCollectionName();
            Collection = db.GetCollection<T>(collectionName);
        }

        public async Task<TimerModel<IEnumerable<T>>> GetManyAsync()
        {
            var timer = new Stopwatch();
            timer.Start();
            var result = await Collection.Find(it => true).ToListAsync();
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<T>>(result, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<T>> UpdateAsync(T item) {
            var timer = new Stopwatch();
            timer.Start();
            await Collection.ReplaceOneAsync(it => it.Id == item.Id, item);
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<T>(item, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<IEnumerable<T>>> InsertManyAsync(IEnumerable<T> items)
        {
            var timer = new Stopwatch();
            timer.Start();
            await Collection.InsertManyAsync(items);
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<T>>(items, timer.Elapsed.TotalMilliseconds);
        }

        public async Task<TimerModel<IEnumerable<string>>> DeleteManyAsync(IEnumerable<string> ids)
        {
            var timer = new Stopwatch();
            timer.Start();
            await Collection.DeleteManyAsync(it => true);
            await Task.WhenAll();
            timer.Stop();
            return new TimerModel<IEnumerable<string>>(ids, timer.Elapsed.TotalMilliseconds);
        }

        public DbSet<T> GetDbSet()
        {
            throw new NotImplementedException();
        }

        public DbContext GetDbContext()
        {
            throw new NotImplementedException();
        }

        public IMongoCollection<T> GetCollection()
        {
            return Collection;
        }

        private static string GetCollectionName() => ((BsonCollectionAttribute)typeof(T).GetCustomAttributes(typeof(BsonCollectionAttribute), true).First()).CollectionName;
    }
}
